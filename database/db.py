from pymongo import MongoClient
import os


client = MongoClient(os.getenv('MONGODB_URI'))
database_name =  os.getenv('MONGODB_NAME')

conn = client[database_name]